Installation
============

::

    $ pip install -r requirements.txt
    $ bower install


Generate HTML
=============

::

    $ cd src/
    $ tacot --autoreload
