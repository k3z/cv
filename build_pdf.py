#!/usr/bin/python
import os
here = lambda x: os.path.join(os.path.abspath(os.path.dirname(__file__)), x)

os.system('wkhtmltopdf -B 7mm -L 0 -R 0 -T 0 --print-media-type --dpi 75 http://cv.k3z.fr/index.html %s' % (
    here('src/_build/cv.pdf')
))
