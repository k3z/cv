#!/usr/bin/python
import os
here = lambda x: os.path.join(os.path.abspath(os.path.dirname(__file__)), x)

os.system("rsync -e 'ssh -p 22' -v -r --exclude='extended/' --delete %s root@k3z:/home/k3zfr/cv/" % here('src/_build/'))
